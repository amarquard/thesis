\section{Targeting DNA repair}

The DNA damage response is a well-coordinated system in healthy cells that functions to maintain the integrity of the genome. Dysregulation of DNA repair systems predisposes to cancer by giving way to the acquisition of genomic aberrations, which in turn can enable the hallmark characteristics necessary for cancer development. But defects in DNA repair systems also bring about sensitivity to DNA-damaging anticancer therapy, because unrepaired DNA lesions induce cell cycle arrest and ultimately apoptosis. Apart from DNA-damaging agents, tumors with DNA repair defects can be treated by targeting compensatory DNA repair pathways, a principle known as synthetic lethality, in which a defect in either repair pathway alone is permissive of cell survival, but inactivation of both causes cell death \citep{Curtin:2012hq}.

\begin{figure}
    \centering
	    \includegraphics[width=0.5\textwidth]{graphics/Birkbak-NtAI.pdf}
    \caption[]{Mechanisms of DNA scarring by telomeric allelic imbalance in \glsentryshort{HR}-defective cells. \textbf{Left:} In the absence of \glsentryshort{HR}, a DNA double strand break may be repaired by \glsentryshort{NHEJ}, which joins the chromatid of one chromosome to that of another (usually nonhomologous) chromosome. After mitotic cell division, this leads to an imbalance in the maternal and paternal alleles present in each daughter cell. \textbf{Right:} Alternatively, a DNA double strand break may be repaired by mitotic recombination, using the homologous chromosome as a template. If \glsentryshort{HR} is defective, the crossover may not be able to be resolved except by extending it to the telomere, generating allelic imbalance of a region extending to the telomere. Modified from \textcite{Birkbak:2012jf}}
    \label{ntai}

	% \floatbox[{\capbeside\thisfloatsetup{capbesideposition={left,top}, capbesidewidth=0.3\textwidth}}]{figure}[\FBwidth]
	% {\caption{A test figure with its caption side by side}\label{ntai}}
	% {\includegraphics[width=0.6\textwidth]{graphics/Birkbak-NtAI.png}}

\end{figure}


\subsection{DNA repair deficiencies}

\Glsentryfull{HR} is a high-fidelity repair mechanism for DNA double strand breaks. BRCA1 and BRCA2 are two important genes in this pathway, and inherited mutations in either gene predispose patients to cancers of the breast, ovaries, pancreas and prostate, with the most striking of those being an 85\% lifetime risk of female breast cancer \citep{Watkins:2014cg}. Inactivation of BRCA1/2 renders the cancer cells deficient in \gls{HR}, with accompanying sensitivity to agents that cause double strand breaks in need of \gls{HR} repair, such as cisplatin and other platinum based agents \citep{Silver:2010hb}, but also to inhibition of the main alternative repair pathway, base excision repair, by use of PARP inhibitors \citep{Lee:2013ji}. The PARP1 protein detects and binds to single strand DNA breaks, and recruits the appropriate repair enzymes. PARP inhibitors such as olaparib prevent the recruitment of repair enzymes, and trap PARP1 in the DNA-bound state, leading to double strand breaks in need of repair by \gls{HR} \citep{Curtin:2012hq}.

Gene expression-based clustering has been applied to define molecular subtypes of sporadic breast cancer, including the poor-prognosis ``basal-like'' or \gls{TNBC}, expressing neither estrogen, progesterone, nor HER2 receptors. An analysis including familial breast cancer in BRCA1/2 mutation carriers showed that these tumors cluster with the \gls{TNBC} subtype, suggesting they share similar etiology \citep{Sorlie:2003cq}. Cisplatin has also been shown to be effective in a subset of \gls{TNBC} patients \citep{Silver:2010hb}, supportive that \gls{HR} deficiency is a common underlying defect, although BRCA1/2 mutations are rare in this group of tumors compared to familial breast cancers. This phenotype of \gls{HR} deficiency but in the absence of BRCA1/2 mutations has been termed ``BRCAness'', and likely reflects that \gls{HR} deficiency can be brought about by inactivation of different members of the \gls{HR} machinery \citep{Turner:2004tg}. The ability to identify the subset of \gls{TNBC} tumors that are defective in \gls{HR} and hence sensitive to platinum agents and PARP inhibitors could greatly improve outcome for these patients.


\subsection{Biomarkers of DNA repair deficiency}

Due to the apparent heterogenous causes of \gls{HR} deficiency, measuring the result rather than the drivers of \gls{HR} deficiency can be used as a surrogate for \gls{HR} status. DNA double strand breaks incorrectly repaired by incompetent \gls{HR} leaves so-called scars in the genome of a cancer cell; in other words, if we know where to look, the cancer genome reveals its own history of \gls{HR} deficiency. Previous work performed in our group defined one such genomic scar biomarker of \gls{HR} deficiency, namely the number of allelic imbalance events that extend to the telomeres \citep{Birkbak:2012jf}. This measure, NtAI, could predict cisplatin sensitivity in breast and ovarian cancers without BRCA1/2 inactivation, and may reflect a likely mechanism for resolving crossover-intermediates from defective \gls{HR}, by which branch migration extends the crossover until it reaches the telomere. In addition, telomeric allelic imbalance events could arise when \gls{NHEJ}, in the absence of an \gls{HR} response, joins chromatid ends from double strand breaks to the wrong chromosomes, resulting in loss or gain of large telomeric segments during mitotic segregation (see \cref{ntai}).


Two other DNA-based scores of \gls{HR} deficiency were published around the same time, developed to predict BRCA1/2 inactivation \citep{Popova:2012fz, Abkevich:2012bt}. A subsequent study applied these three scores to a cohort of breast cancer samples of the different subtypes, and found that all three scores associated with BRCA1/2 inactivation, which was assessed by both mutational status, promoter methylation, and copy number loss \citep{Timms:2014fv}. A combined score, derived as the mean of the three scores, added significant information of BRCA1/2 inactivation compared to either score alone.  Importantly, they observed samples with high scores among the samples with intact BRCA genes, suggesting a proportion of tumors have other defects in \gls{HR}, that are picked up by the genomic scar biomarkers, but overlooked when focusing on BRCA1/2 activity alone \citep{Timms:2014fv}.


\begin{tcolorbox}[title=Paper I]
This study compares the three \gls{HR} deficiency scores, and applies them to more than 5000 tumors of the 15 cancer types available from \gls{TCGA} at the time of the study. We show that there is a subpopulation of tumors with high \gls{HR} deficiency scores within 12 of these cancer types, meaning a number of patients who would not be offered these treatments as standard-of-care could very likely benefit from treatment with platinum agents or PARP inhibitors. This study is a very good example of the value of data-sharing projects such as the \gls{TCGA}, that allow us to analyse genomic aberrations across tumor types, providing insights about how to extend therapies effective in one tumour type to tumours of other organs but with a similar genomic profile \citep{Weinstein:2013jp}.
\end{tcolorbox}

% could be combined in one model (machine learning?)
% ‘signature 3’ from the mutational sigs paper probably also measures \gls{HR} deficiency \citep{Watkins:2014cg}



One drawback of using genomic scars as surrogates for \gls{HR} status is that they are a measure of all historic exposure to defective \gls{HR}, but not necessarily the current status of \gls{HR}. In the case where new mutations can restore \gls{HR}, leading to resistance to platinum and PARP inhibitors, such a tumor would still be classified as sensitive, and treatment could fail. This means genomic scars as predictive biomarkers for platinum or PARP inhibitor therapy have very good negative predictive value (NPV), but lower positive predictive value (PPV), and might well benefit from being combined with a predictor of resistance to therapy, specifically trained on tumors with high genomic scar scores, see \cref{watkins} \citep{Watkins:2014cg}.

\begin{figure}[htbp]
    \centering
	    \includegraphics[width=0.9\textwidth]{graphics/Watkins2014-biomarkers-vertical.pdf}
    \caption[]{Combining a genomic scar biomarker with a resistance biomarker. \textbf{Top:} A genomic scar biomarker used to identify responders to genotoxic drugs or drugs that target the HR machinery is expected to have high \gls{NPV}, meaning that non-responders are correctly identified. But due to the emergence of resistance which can occur after the DNA scars have been formed on the genome, these biomarkers may have low \gls{PPV}. \textbf{Bottom:} If the genomic scar biomarker could be combined with a biomarker able to identify resistant cases among the predicted responders, this could result in a combined biomarker score with both good \gls{NPV} and \gls{PPV}. Modified from \textcite{Watkins:2014cg}}
    \label{watkins}
\end{figure}

% Other examples of a posibility to couple a genomic scar biomarker to treatment, is Signature 6 (mismatch repair deficiency) and methotrexate
% {Watkins:2014tk}

% tobacco smoke and genomic scaring
% mutational signatures




