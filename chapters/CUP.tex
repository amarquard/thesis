Most cancer patients present at diagnosis with their primary tumor. However, in 10-15\% of cancer cases, patients present with metastatic disease, and in some of these cases the location of the primary tumor is not obvious. These \glspl{MUO} are then subject to additional tests, and if the primary tumor is still not located, the diagnosis of \gls{CUP} is given. Data from the UK in 2012\footnote{\url{http://www.cancerresearchuk.org/health-professional/cancer-statistics}} show that although \gls{CUP} only accounted for 2-3\% of all cancer cases, it was the fourth most common cause of death from cancer (see \cref{CancerUK}). Concordantly, the median survival time for patients with \gls{CUP} is less than a year, by some estimates down to 3 to 4 months. There is an obvious urgent need to improve the outcome for patients with \gls{CUP}.
\vfill
\begin{figure}[htbp]
    \centering
	    \includegraphics[width=\textwidth]{graphics/Cancer_UK_2012_top20deaths_recolor2.pdf}
    \caption[]{The 20 cancer types causing the highest numbers of deaths in the United Kingdom in 2012. \gls{CUP} was the 4th most common cause of cancer-related death. Based on data from http://www.cancerresearchuk.org/health-professional/cancer-statistics}
    \label{CancerUK}
\end{figure}


\section{Diagnosis}
When a patient presents with suspected metastatic disease, a biopsy is taken for histomorphological examination, in order to confirm the presence of a malignant lesion, and to establish that the lesion is not a local primary tumor. Then, with the aid of \gls{IHC}, the tumor type and subtype is determined, the four main types being adenocarcinoma, undifferentiated carcinoma, squamous cell carcinoma and melanoma. \gls{IHC} is also used to search for a putative tissue of origin, by staining with organ-specific markers. Unfortunately, organ-specific markers are not currently available for all types of cancer, and aberrant expression patterns in tumors may complicate this approach in some cases. Any findings from \gls{IHC} as well as other tests including a thorough history and physical examination and CT scans of the chest, abdomen and pelvis can prompt further tests if a primary site is suspected, such as endoscopy or mammography  \citep{Kramer:2015uk}, see \cref{muo}.

\begin{figure}[htbp]
    \centering
	    \includegraphics[width=\textwidth]{graphics/muo.pdf}
    \caption[]{Diagnostic workup of \glspl{MUO} and \glspl{CUP}. Roughly 5\% of all patients presenting with a tumor are given the tentative diagnosis of \glsentryfull{MUO}. After going through expensive, time-consuming and often invasive tests to locate the primary tumor, such as those listed in the green box, approximately 2-4\% of all new cancer cases are given the diagnosis of \glsentryfull{CUP}}
    \label{muo}
\end{figure}


\section{Treatment}
If a primary tumor is identified, the patient is offered organ-specific therapy. Eventhough a primary tumor cannot be anatomically located, there are several favourable subsets of \glspl{CUP}, for which there are established treatment regimens. These subsets have distinct phenotypes that suggest a certain primary site, and the treatment outcome for these subsets is comparable to the corresponding cancers of known primary \citep{Hainsworth:2013kn}. About 20\% of all \gls{CUP} patients can be assigned to a favourable subset \citep{Kramer:2015uk}.
Otherwise, the patient is given empirical treatment for \gls{CUP}, which is most commonly a taxane/platinum combination, although the benefits are limited with median survival time still below 1 year. Although the effectiveness of organ-specific therapy varies, it gives an overall better outcome than empirical \gls{CUP} treatment, advocating the importance of inferring the primary sites of \glspl{CUP}
\citep{Hainsworth:2013kn}.

\section{Molecular assays}
Several assays have been developed that use gene expression profiles to identify the primary site of a \gls{CUP}. The three most widely used are the 92-gene RT-PCR assay CancerTYPE ID \citep{Erlander:2011hx}, the microarray-based Tissue of Origin test \citep{Monzon:2009fg} and a microRNA microarray-based test \citep{Rosenfeld:2008ci,Kramer:2015uk}. Due to the occult nature of the primary tumor there is no direct way to assess the accuracy of the use of these assays for \gls{CUP} diagnosis, but indirect methods, such as confirmatory \gls{IHC} or clinicopathologic review, estimated an accuracy of 75\% for the 92-gene assay \citep{Greco:2013he}. Importantly, molecular classifier assays such as the 92-gene assay are not used alone to give a diagnosis, but always in concert with all other relevant data such as clinical information, histology and \gls{IHC} staining results.
A recent study at Rigshospitalet in Copenhagen developed a gene expression-based classifier for \glspl{CUP}, which was trained on data from normal tissue, primary tumors and metastases of known origin, with a reported accuracy on the validation set of 87\% \citep{Vikesa:2015ch}. They then used this classifier to infer the primary site of 57 \glspl{CUP}, of which 28 patients had a possible primary site identified during the diagnostic work-up. In about half of those cases there was agreement with the gene expression-based classification, suggesting that the accuracy of such assays on actual \glspl{CUP} is lower than for samples with known origin.
Another approach to provide better treatment choices for \glspl{CUP} would be to target specific molecular alterations in the tumor, rather than treating according to primary site. A study to address this strategy applied targeted sequencing of 701 cancer-associated genes to 16 \gls{CUP} tumors, and found targetable mutations and copy number alterations in 12 cases \citep{Tothill:2013cq}.

Many of the diagnostic tools available to identify the origin of \glspl{CUP}, including \gls{IHC} and molecular classifiers, have the underlying assumption that \glspl{CUP} and their counterparts with known origin share a similar biology and phenotype. This question remains unanswered, but analyses of mutational landscapes in \glspl{CUP} by \gls{NGS} showed mutation frequencies in cancer genes similar to those seen in other cancers \citep{Ross:2015dn}, consistent with the notion that there is no distinct \gls{CUP} biology, and compatible with the hypothesis that mutational landscapes in \glspl{CUP} simply reflect their tissue of origin.

\begin{tcolorbox}[title=Paper II]
This study describes a method to predict the origin of a tumor from its somatic mutations. It was developed with \glspl{CUP} in mind, as a tool to guide diagnosis by prioritising the putative primary sites to investigate. For many patients with suspected \gls{CUP} their survival time is short, perhaps only a few months, and exhaustive testing by \gls{IHC}, imaging and endoscopies can take much or all of this precious time. Classification tools like that described in our paper could therefore be clinically useful, if by prioritisation of these tests they could lead to faster diagnoses.
\end{tcolorbox}


% Dietlein et al
% Paper from Rigshospitalet
% Emphasize strict data split (train, test, validate)



\section{CUP biology}
Autopsy data have shown that in one fourth of autopsied \gls{CUP} cases a primary tumor was never identified \citep{Pentheroudakis:2007he}, suggesting that in some cases the primary tumor is not present for various reasons. Some explanations for this phenomenon include that the primary has disappeared due to a growth disadvantage, either intrinsic or due to immune targeting, or that metastatic spreading occurred before the primary lesion became malignant. The latter explanation goes against the classical step-wise model of malignant transformation, where tissue invasion and metastatic spreading are the last steps in tumor evolution \citep{Kramer:2015uk}. Nevertheless, a study of 18 \gls{CUP} patients of suspected head and neck origin, found genetic alterations in histologically benign mucosal biopsies in 10 patients, where the genetic alterations matched those in the lymph node metastases \citep{Califano:1999vw}.

The current understanding of the biology of \gls{CUP} is that in some cases the metastatic lesions are genetically and phenotypically similar to their counterparts with known origin, demonstrated so far by the favourable subsets of \glspl{CUP} for which organ-specific therapy has some efficacy. Other cases, however, are probably ``true'' \glspl{CUP}, that share some underlying biology which promotes early spreading and a growth disadvantage of the primary. A primary site classifier trained on primary tumors, such as that described in Paper II, should by these definitions have high accuracy in the group of \glspl{CUP} that resemble their primaries. It would therefore be valuable to decipher a biomarker or signature of the ``true'' \gls{CUP} subtype, and subsequently direct alternative classification schemes to this group of tumors.

% Perhaps don't need origin but actionable mutations (ref)
Finally, it is currently not fully evident whether identifying the primary site of a \gls{CUP} is the only way to improve outcome for the patient. A recent analysis of 200 \glspl{CUP} by \textcite{Ross:2015dn} found an actionable genomic alteration for which there is a drug available in 85\% of the samples, suggesting a new approach for treatment selection in \glspl{CUP} other than putative primary site-directed therapy.