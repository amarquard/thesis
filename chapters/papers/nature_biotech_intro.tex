% Paper intro: discuss motivation for our specific approach
This project started in 2014, as a collaboration with the lab of Sine Reker Hadrup at Section for Immunology and Vaccinology, the Technical University of Denmark. They were working on a novel idea to enable high-throughput screening for T-cell recognition of >1000 peptides simultaneously, by labelling the peptides with DNA barcodes. The barcodes could then be subject to sequencing, and a count of the number of reads present from each barcode would indicate the abundance of each peptide within the pool of peptides recognised by T-cells. Amalie Kai Bentzen developed the experimental protocol, and I developed the computational protocol for recovery of DNA barcodes from the sequencing results, and translation of this output into T-cell specificities. The method is currently being used in their lab, and now that the method has been published --- and presented at numerous cancer and immunological meetings --- we anticipate that many research labs will begin to transition from the current methods based on fluorescent or metal tags to the DNA barcoding approach.

Because the method requires only small amounts of biological sample, and is high-throughput on large peptide libraries, it is ideally suited for personalized approaches, such as an individualized screen for T-cell recognition of potential neoepitopes identified for each cancer patient from sequencing of a tumor biopsy.

The manuscript published in Nature Biotechnology is brief on details regarding the computational analysis. In light hereof the following section will elaborate on some of our considerations during the design and implementation of the analysis of DNA barcode reads.


\section{DNA-encoded library screening}
We used synthetic DNA barcodes to label individual peptides in the peptide libraries used to screen for T-cell binding. After enrichment of peptide-\gls{MHC} multimers bound to T-cells, the identity of the peptide is revealed by sequencing the DNA barcode and referring to a table of the barcode-peptide encodings used in the specific experiment.

\subsection{Barcode design}

The DNA barcodes were designed to contain shared primer regions, used to amplify the barcodes before sequencing, along with a tag that is specific for each peptide. Here the word \emph{tag} is used to describe a defined stretch of nucleotides of a known sequence. To minimize the number of different synthetic oligonucleotides required to encode large peptide libraries, and thereby reducing purchasing costs and handling time, the barcodes were designed in a two-part combinatorial fashion. Two separate DNA tags (A and B) are combined in an initial annealing and elongation step, and each peptide is then encoded by a specific combination of an A and a B tag, which we refer to as the AxBy tag (see \cref{barcode-overview}a). In this way a library of more than 1000 peptides could be encoded using only 22 different A tags and 55 B tags.

% \begin{figure}
%     \centering
% 	    \includegraphics[width=\textwidth]{graphics/barcode-design.pdf}
%     \caption[Short caption]{Long caption.}
%     \label{barcode}
% \end{figure}

\begin{figure}
    \centering
	    \includegraphics[width=\textwidth]{graphics/barcode-design-and-analysis-2.pdf}
    \caption[]{Design and analysis of the DNA barcodes. \textbf{a:} The structure of the DNA barcode, with the nucleotide lengths shown below each segment. The blue segment is used as a multiplexing barcode; red segments are shared and so have the same sequence in all barcodes; yellow segments are the UMIs, which are different for each initial barcode molecule, but identical within clones of barcodes generated during PCR; green segments make up the AxBy combination used to identify each peptide included in the library. \textbf{b:} The brown rectangle represents a sequencing read, and the colored shaded areas represent successful alignment of each barcode segment to the read. The number printed in each segment is the number of different sequences that need to be searched to find the correct alignment and identify the sample or peptide. For further details, see the main text page \pageref{text-barcode-analysis}}
    \label{barcode-overview}
\end{figure}

PCR amplification steps generally confer a high risk of results being confounded by cross-contamination, since even just a few DNA molecules carried over from one experiment to the next can give a high signal after amplification. Furthermore, biases in amplification efficiency, and oversampling during sequencing, contribute a bias to the final abundances of each DNA barcode sequence. To eliminate these effects, a short stretch of random nucleotides is included in the DNA barcode design, denoted a \gls{UMI} \citep{Kivioja:2011kg}. These nucleotides are incorporated randomly for each molecule during synthesis of the barcodes. After the sequencing step the \gls{UMI} can be used to identify clones of sequences that originated from the same molecule, either by clonal amplification or because that specific sequence generated a high number of reads. In addition, by tracking the \gls{UMI} sequences from experiment to experiment, carry-over contamination can be identified early on.

Finally, for parallel analysis of multiple samples in one reaction, a multiplexing tag is added to each DNA barcode during amplification. This sequence is denoted the sample ID, and is used to demultiplex the sequencing reads.

\subsection{Analysis of DNA barcode data}
The use of DNA barcodes to label large screening libraries has previously been reported in the field of drug discovery, where chemical libraries are screened for drug activity \citep{Brenner:1992vo,Mannocci:2008uq}. In addition, the generation of a library of yeast mutant strains has been reported, in which identifiable DNA barcodes (2 x 20 nts) are cloned into each strain along with the specific mutated gene \citep{Ho:2009gm}. Initially a microarray-based approach was used to generate a readout from screening this yeast library, but a sequencing-based readout has since been described. In this approach, they mapped each sequencing read to a database of anticipated sequences using alignment software, and the number of reads mapping to each barcode was used to indicate the abundance of each yeast strain \citep{Smith:2009kr}.

We considered a similar method to generate a readout from the DNA-encoded peptide screens by doing full-length alignment, but abandoned this approach for several reasons. Firstly, our DNA barcodes are a lot longer than those used in the yeast library, and mapping each read in its full length would be relatively slow.
\label{text-seq-errors}More importantly, it is noteworthy that the different parts of the barcode are not equally important when evaluating an alignment. If a read can be aligned to more than one barcode --- which is highly likely given the common primer and annealing sequences, and the A and B tags that are shared with multiple other barcodes --- good practice would be to select the barcode that gave the highest alignment score, discarding any alignments that do not reach a defined minimum threshold score. This practice risks discarding potentially informative reads, in the context of our particular DNA barcodes. Since we are sequencing synthetic DNA of a known sequence, all reads should map perfectly to one barcode, except for amplification and sequencing errors. Now consider two reads that have the same number of errors, and should therefore align to their respective barcode sequences with roughly the same alignment score. If the errors in read 1 are mainly in the common regions (primers and annealing region), the tags used to identify the sample and the peptide may still be intact, and this read can confidently be assigned to its barcode (see \cref{barcode-scores}). On the contrary, if the errors in read 2 are mainly in the sample tag and/or the A and B tags, there may be too many errors to confidently assign the read to a specific sample and peptide, and the read should be discarded. Hence, a hard cut-off on the alignment score is not an ideal way to separate valid and invalid alignments in this case.

\begin{figure}
    \centering
	    \includegraphics[width=\textwidth]{graphics/barcode-alignment-scoring-v2.pdf}
    \caption[]{The effect of sequencing errors on the barcode analysis varies depending on the position of the errors. The brown rectangle represents a sequencing read, and stars each represent one sequencing error. For details, see the main text page \pageref{text-seq-errors}}
    \label{barcode-scores}
\end{figure}



To honour that some parts of the barcode are more important to accurately assign each read to a barcode, I instead chose an approach where each part of the barcode is mapped independently.\label{text-barcode-analysis} In the first step, each read is aligned separately to the forward primer, the annealing region and the reverse primer. In order to ignore reads that are truncated or contain too many errors, further analysis is only performed on reads aligning with a specified minimum alignment score to at least 2 of the 3 common sequences (see \cref{barcode-overview}b). Next, the filtered reads are aligned to the libraries of A and B sequences, to identify which AxBy combination the read consists of. Then, the mapping positions of the A and B tags and of the primers are used to determine the location of the sample ID and the \gls{UMI} sequences. The sample ID sequence is isolated and aligned to the library of sample IDs using the Needleman-Wunsch global alignment algorithm \citep{Needleman:1970vq}, and the highest scoring sample ID wins. The \gls{UMI} sequences are isolated and concatenated, and used to perform clonality reduction on the final abundance readout. This means that instead of counting all reads per barcode, only the number of unique \glspl{UMI} found per barcode are used to calculate the abundance of each peptide.

The sample identifier could alternatively be analysed using existing demultiplexing tools designed for multiplex \gls{NGS} data, but in our experience demultiplexing tools are not flexible enough for our purpose, and would result in loss of potentially informative reads. We often found a few sequencing errors such as missing bases from the 5' end of the read, which prevented complete demultiplexing. But because the sequences are synthetic and not biological, we can benefit from knowing the overall structure of each read, and hence using the position from aligning the forward primer to guide the dissection of the sample ID sequence is likely to give higher yields of useful data per sequencing run. 

% how they have been analyzed previously?
% dna-encoded chemical libraries


% "Bar-seq", mapping reads to a database of anticipated sequences, using MAQ software (pre BWA tool)  \citep{Smith:2009kr}
% 
% Demultiplexing tools designed for multiplex sequencing data
%  There are a bunch of tools (https://omictools.com/demultiplexing-category)
%  And Simon has a pythin script (I got it from Jens Friis), which seems to basically ccalcualte the edit distance, and there's a max edit distance to assign a sequence to a multiplexing barcode. I think this is pretty similar to what I ended up doing.
% 


% amplifiable tag for identification
%
% counts indicate the abundance
%
% "input" sample, refer to it as "library before selection" and then the other samples are "after selection" ? 
%
% How many sequences could we assign to something?