## Run these things manually

######################
# Installing XeLaTex #
######################

## Download BasicTex from here:
# http://www.texts.io/support/0001/
# This includes xelatex

## Next, install latexmk
# https://www.ctan.org/tex-archive/support/latexmk
# Don't download this, but use the automatic package installer that came with Tex:
# (needs to be run as sudo)
sudo tlmgr install latexmk

## Install other packages/style files as well using tlmgr:
sudo tlmgr install nag
sudo tlmgr install csquotes
sudo tlmgr install biblatex
sudo tlmgr install logreq
sudo tlmgr install xstring
sudo tlmgr install cleveref
sudo tlmgr install totcount
sudo tlmgr install blindtext
# sudo tlmgr install algorithm
sudo tlmgr install tex-gyre     # fonts
sudo tlmgr install biber

sudo tlmgr install biblatex-nature
sudo tlmgr install pdfpages
sudo tlmgr install tcolorbox
sudo tlmgr install environ      # required by tcolorbox, but not installed with it
sudo tlmgr install trimspaces   # required by tcolorbox, but not installed with it

sudo tlmgr install floatrow   # captions next to figures


## Run 'make' twice to get the bbl file (bibliography)


# Abbreviations:
sudo tlmgr install glossaries
sudo tlmgr install mfirstuc       # needed by glossaries
sudo tlmgr install xfor           # needed by glossaries
sudo tlmgr install datatool  # needed by glossaries
sudo tlmgr install substr
sudo tlmgr install tracklang

# Helps automatically install the needed packages for a certain tex document
sudo tlmgr install texliveonfly
# now run: texliveonfly document.tex
# can't get it to work!
